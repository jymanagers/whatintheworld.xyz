const witw = "witw-site-v1"
  const assets = [
    "/",
    "/main.html",
    // include js, css, icons, other assets
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(witw).then(cache => {
        cache.addAll(assets)
    })
  )
})

  self.addEventListener("fetch", fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)
    })
  )
  })