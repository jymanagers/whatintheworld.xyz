if ("serviceWorker" in navigator) {
    window.addEventListener("load", function () {
      navigator.serviceWorker
        .register("/serviceWorker.js")
        .then(res => console.log("service worker registered"))
        .catch(err => console.log("service worker not registered", err))
    })
}

let curTab = "Google Drive";
let searchOptions;
const baseUrl = "https://www.google.com/search?q=";

$("#tab-info a").on("click", function (e) {
    e.preventDefault();
    $(this).tab('show');
    curTab = this["text"];
})

$("#submitBtn").on("click", function (e) {
    e.preventDefault();

    // TODO: CHECK FORM VALID
    let userInput = $("#inputSearch").val();

    // CHECK TAB
    if (curTab == "Books") {
        search_books(userInput);
    } else if (curTab == "Google Drive") {
        search_google_drive(userInput);
    } else if (curTab == "Research Papers") {
        search_papers(userInput);
    };
});


function open_new_tab(itemArray) {
    let counter;
    for (counter = 0; counter < itemArray.length; ++counter) {
        window.open(itemArray[counter], '_blank');
    };
}

function search_google_drive(userInput) {
    // CHECK GOOGLE DRIVE OPTIONS
    var googleDriveOptionsArray = [];

    var isTypeFile = $("#typeFile").prop("checked");
    var isTypeDocument = $("#typeDocument").prop("checked");
    var isTypePresentation = $("#typePresentation").prop("checked");
    var isTypeSpreadsheet = $("#typeSpreadsheet").prop("checked");

    if (isTypeFile) {
    searchOptions = " site:https://drive.google.com/file/d";
    searchUrl = baseUrl + userInput + searchOptions;
    googleDriveOptionsArray.push(searchUrl);
    }
    if (isTypeDocument) {
    searchOptions = " site:https://docs.google.com/document/d";
    searchUrl = baseUrl + userInput + searchOptions;
    googleDriveOptionsArray.push(searchUrl);
    }
    if (isTypePresentation) {
    searchOptions = " site:https://docs.google.com/presentation/d";
    searchUrl = baseUrl + userInput + searchOptions;
    googleDriveOptionsArray.push(searchUrl);
    }
    if (isTypeSpreadsheet) {
    searchOptions = " site:https://docs.google.com/spreadsheet/d";
    searchUrl = baseUrl + userInput + searchOptions;
    googleDriveOptionsArray.push(searchUrl);
    }

    if (googleDriveOptionsArray.length == 0) {
    alert("YOU MUST SELECT ONE GOOGLE DRIVE TYPE");
    return false;
    }
    if (userInput == "") {
    alert("YOU MUST INPUT YOUR QUERY");
    return false;
    }

    open_new_tab(googleDriveOptionsArray);
}

function search_books(userInput){

    booksOptionsArray = [];

    var isTypeGoogle = $("#typeGoogle").prop("checked");
    var isTypeBookfi = $("#typeBookfi").prop("checked");
    var isTypeLibgen = $("#typeLibgen").prop("checked");
    var isTypeZlib = $("#typeZlib").prop("checked");

    if (isTypeGoogle) {
        searchOptions = " filetype:pdf -inurl:(jsp|pl|php|html|aspx|htm|cf|shtml)";
        searchUrl = baseUrl + userInput + searchOptions;
        booksOptionsArray.push(searchUrl);
    }
    if (isTypeBookfi) {
        searchUrl = "http://en.bookfi.net/s/?t=0&q=" + userInput;
        booksOptionsArray.push(searchUrl);
    }
    if (isTypeLibgen) {
        searchUrl = "http://gen.lib.rus.ec/search.php?open=0&res=100&view=simple&phrase=1&column=def&req=" + userInput;
        booksOptionsArray.push(searchUrl);
    }
    if (isTypeZlib) {
        searchUrl = "https://b-ok.cc/s/" + userInput;
        booksOptionsArray.push(searchUrl);
    }

    if (booksOptionsArray.length == 0) {
        alert("YOU MUST SELECT ONE SOURCE TO SEARCH FROM");
        return false;
    }
    if (userInput == "") {
        alert("YOU MUST INPUT YOUR QUERY");
        return false;
    }

    open_new_tab(booksOptionsArray);
};

function search_papers(userInput){

    papersOptionsArray = [];

    var isTypeArxiv = $("#typeArxiv").prop("checked");
    var isTypeBooksc = $("#typeBooksc").prop("checked");
    var isTypeGoogleScholar = $("#typeGoogleScholar").prop("checked");
    var isTypeNcbi = $("#typeNcbi").prop("checked");
    var isTypeResearchGate = $("#typeResearchGate").prop("checked");
    var isTypeJstor = $("#typeJstor").prop("checked");
    var isTypeChemRxiv = $("#typeChemRxiv").prop("checked");
    var isTypePubChem = $("#typePubChem").prop("checked");
    var isTypeMedRxiv = $("#typeMedRxiv").prop("checked");
    var isTypePubMed = $("#typePubMed").prop("checked");
    var isTypePsyArXiv = $("#typePsyArXiv").prop("checked");
    var isTypePubPsych = $("#typePubPsych").prop("checked");    
    var isTypeAstrophysics = $("#typeAstrophysics").prop("checked");    
    var isTypeSocialSciences = $("#typeSocialSciences").prop("checked");
    var isTypeBiology = $("#typeBiology").prop("checked");
    var isTypeTechnology = $("#typeTechnology").prop("checked");

    if (isTypeArxiv) {
        searchUrl = "https://arxiv.org/search/?query=" + userInput + "&searchtype=all&source=header";
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeBooksc) {
        searchUrl = "https://booksc.xyz/s/" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeGoogleScholar) {
        searchUrl = "https://scholar.google.com/scholar?q=" + userInput
        papersOptionsArray.push(searchUrl)
    }
    if (isTypeResearchGate) {
        searchUrl = "https://www.researchgate.net/search?q=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeJstor) {
        searchUrl = "https://www.jstor.org/action/doBasicSearch?Query=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeChemRxiv) {
        searchUrl = "https://chemrxiv.org/?q=" + userInput + "&searchMode=1";
        papersOptionsArray.push(searchUrl);
    }
    if (isTypePubChem) {
        searchUrl = "https://pubchem.ncbi.nlm.nih.gov/#query=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeMedRxiv) {
        searchUrl = "https://www.medrxiv.org/search/" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypePubMed) {
        searchUrl = "https://www.ncbi.nlm.nih.gov/pubmed/?term=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeNcbi) {
        searchUrl = "https://www.ncbi.nlm.nih.gov/search/all/?term=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypePsyArXiv) {
        searchUrl = "https://psyarxiv.com/discover?q=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypePubPsych) {
        searchUrl = "https://pubpsych.zpid.de/pubpsych/Search.action?q=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeAstrophysics) {
        searchUrl = "https://ui.adsabs.harvard.edu/search/q=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeSocialSciences) {
        searchUrl = "https://osf.io/preprints/socarxiv/discover?q=" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeBiology) {
        searchUrl = "https://www.biorxiv.org/search/" + userInput;
        papersOptionsArray.push(searchUrl);
    }
    if (isTypeTechnology) {
        searchUrl = "https://dl.acm.org/action/doSearch?AllField=" + userInput
        papersOptionsArray.push(searchUrl);
    }

    if (papersOptionsArray.length == 0) {
        alert("YOU MUST SELECT ONE SOURCE TO SEARCH FROM");
        return false;
    }
    if (userInput == "") {
        alert("YOU MUST INPUT YOUR QUERY");
        return false;
    }

    open_new_tab(papersOptionsArray);
};

document.onkeydown = function(e) {
    if ((e.ctrlKey && 
        (e.keyCode === 83 ||
        e.keyCode === 85 || 
        e.keyCode === 117)) || (e.keyCode === 123)) {
        return false;
    }
    else {
        return true;
    }
};